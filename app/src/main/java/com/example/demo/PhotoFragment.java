package com.example.demo;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Created by holod on 21.06.17.
 */

public class PhotoFragment extends Fragment {
    private static final int CAMERA_REQUEST = 123;
    private final static Logger LOGGER = Logger.getLogger(PhotoFragment.class.getName());
    private static final String DATA = "data";
    private Button fragment_photo_bt;
    private ImageView fragment_photo_iv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo, null, false);
        initFields(view);
        return view;
    }

    private void initFields(View view) {
        fragment_photo_bt = (Button) view.findViewById(R.id.fragment_photo_bt);
        fragment_photo_iv = (ImageView) view.findViewById(R.id.fragment_photo_iv);
        initActions();

    }

    private void initActions() {
        fragment_photo_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(getActivity()).setTitle(getActivity().getResources()
                        .getString(R.string.make_photo)).setMessage(getActivity().getResources()
                        .getString(R.string.really_make_photo)).setNegativeButton(android.R
                        .string.cancel, null).setPositiveButton(R.string.make_photo, new
                        DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent cameraIntent = new Intent(android.provider.MediaStore
                                .ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cameraIntent, CAMERA_REQUEST);
                    }
                }).create().show();

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            try {
                Bitmap photo = (Bitmap) data.getExtras().get(DATA);
                fragment_photo_iv.setImageBitmap(photo);
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
            }
        }
    }

}
