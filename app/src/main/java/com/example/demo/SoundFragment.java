package com.example.demo;

import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by holod on 21.06.17.
 */

public class SoundFragment extends Fragment {
    private final static Logger LOGGER = Logger.getLogger(SoundFragment.class.getName());
    private Button fragment_click_bt;
    private AudioManager audioManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
            savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sound, null, false);
        initFields(view);
        return view;
    }

    private void initFields(View view) {
        fragment_click_bt = (Button) view.findViewById(R.id.fragment_click_bt);
        audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        initActions();

    }

    private void initActions() {
        fragment_click_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    audioManager.playSoundEffect(AudioManager.FX_KEY_CLICK, 1);
                } catch (Exception e) {
                    LOGGER.log(Level.SEVERE, e.getMessage(), e);
                }
            }
        });
    }
}
